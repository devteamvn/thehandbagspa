<?php
add_action('after_setup_theme', 'blankslate_setup');
function blankslate_setup() {
	load_theme_textdomain('blankslate', get_template_directory() . '/languages');
	add_theme_support('title-tag');
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');
	global $content_width;
	if (!isset($content_width)) {
		$content_width = 640;
	}

	register_nav_menus(
		array('main-menu' => __('Main Menu', 'blankslate'))
	);
}
add_action('wp_enqueue_scripts', 'blankslate_load_scripts');
function blankslate_load_scripts() {
	wp_enqueue_script('jquery');
}
add_action('comment_form_before', 'blankslate_enqueue_comment_reply_script');
function blankslate_enqueue_comment_reply_script() {
	if (get_option('thread_comments')) {wp_enqueue_script('comment-reply');}
}
add_filter('the_title', 'blankslate_title');
function blankslate_title($title) {
	if ($title == '') {
		return '&rarr;';
	} else {
		return $title;
	}
}
add_filter('wp_title', 'blankslate_filter_wp_title');
function blankslate_filter_wp_title($title) {
	return $title . esc_attr(get_bloginfo('name'));
}
add_action('widgets_init', 'blankslate_widgets_init');
function blankslate_widgets_init() {
	register_sidebar(array(
		'name' => __('Sidebar Widget Area', 'blankslate'),
		'id' => 'primary-widget-area',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => "</li>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}
function blankslate_custom_pings($comment) {
	$GLOBALS['comment'] = $comment;
	?>
<li <?php comment_class();?> id="li-comment-<?php comment_ID();?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter('get_comments_number', 'blankslate_comments_number');
function blankslate_comments_number($count) {
	if (!is_admin()) {
		global $id;
		$comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
		return count($comments_by_type['comment']);
	} else {
		return $count;
	}
}
function life4code__widgets_init() {
	register_sidebar(array(
		'name' => 'Homepage Blocks',
		'id' => 'homepage',
		'description' => 'Home page block',
		'before_widget' => '<div class="home-block">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
add_action('widgets_init', 'life4code__widgets_init');

// Creating the widget
class home_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'home_widget',

			// Widget name will appear in UI
			__('Home Widget', 'home_widget_domain'),

			// Widget description
			array('description' => __('Home widgets', 'home_widget_domain'))
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget($args, $instance) {
		$args['before_title'] = '<div class="title-bar fadesc" style="opacity: 1;">
          <aside>
          <h2>';
        $args['after_title'] = '</h2>
          </aside>
     </div>';
		$title = apply_filters('widget_title', $instance['title']);
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if (!empty($title)) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// This is where you run the code and display the output
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form($instance) {
		if (isset($instance['title'])) {
			$title = $instance['title'];
		} else {
			$title = __('New title', 'home_widget_domain');
		}
	// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:');?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>" type="text"><?php echo esc_attr($title); ?></textarea>
		</p>
		<?php
	}

	// Updating widget replacing old instances with new
	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
		return $instance;
	}
} // Class home_widget ends here

// Register and load the widget
function home_load_widget() {
	register_widget('home_widget');
}
add_action('widgets_init', 'home_load_widget');

remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);

foreach ( array( 'pre_term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_kses_data' );
}

remove_action( 'woocommerce_before_shop_loop_item_title', 	'woocommerce_template_loop_product_thumbnail', 10 );


// Custom

// Shop Item Thumbnail
function aurum_shop_loop_item_thumbnail() {
	echo '
            <div class="panel-body text-center product-body">
              <a itemprop="url" href="' . get_permalink() . '">';
              	echo woocommerce_get_product_thumbnail(apply_filters('laborator_wc_product_loop_thumb_size', 'shop-thumb')) . '</a><br>';
}

add_action( 'woocommerce_before_shop_loop_item', 'aurum_shop_loop_item_thumbnail' );

// Remove Item Link
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );


add_action( 'woocommerce_shop_loop_item_title', 'aurum_shop_loop_item_title', 5 );
// Show Product Title (Loop)
function aurum_shop_loop_item_title() {
	?>
	<a class="info" itemprop="name" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
	<?php
}
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

function wc_radio_variation_attribute_options( $args = array() ) {
	$args = wp_parse_args( apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ), array(
		'options'          => false,
		'attribute'        => false,
		'product'          => false,
		'selected' 	       => false,
		'name'             => '',
		'id'               => '',
		'class'            => '',
		'show_option_none' => __( 'Choose an option', 'woocommerce' )
	) );

	$options   = $args['options'];
	$product   = $args['product'];
	$attribute = $args['attribute'];
	$name      = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );

	if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
		$attributes = $product->get_variation_attributes();
		$options    = $attributes[ $attribute ];
	}

	$html = '<ul class="list-group">';

	if ( ! empty( $options ) ) {
		if ( $product && taxonomy_exists( $attribute ) ) {
			// Get terms if this is a taxonomy - ordered. We need the names too.
			$terms = wc_get_product_terms( $product->id, $attribute, array( 'fields' => 'all' ) );

			foreach ( $terms as $term ) {
				if ( in_array( $term->slug, $options ) ) {
					$html .= '<li>
						<input type="radio" name="' . esc_attr( $name ) . '" value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>
							<label>
								<span class="variant-description">
									' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '
								</span>
							</label>
						</li>';
				}
			}
		} else {
			foreach ( $options as $option ) {
				// This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
				$selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
				$html .= '<li>
					<input type="radio" name="' . esc_attr( $name ) . '" value="' . esc_attr( $option ) . '" ' . $selected . '>
						<label>
							<span class="variant-description">
								' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) ) . '
							</span>
						</label>
					</li>';
			}
		}
	}

	$html .= '</ul>';

	echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args );
}