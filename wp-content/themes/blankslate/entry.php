<article class="grid-item" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<figure><a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url() ?>"></a></figure>
	<aside>
		<div class="txt2">
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<p><?php echo get_post_custom_values('summary')[0] ?></p>
		</div>
		<a href="<?php the_permalink(); ?>" class="btn">READ MORE</a>
		<p> <?php echo get_post_time('d/m/Y'); ?> </p>
	</aside>
</article>