<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div
  data-hook="product_show"
  itemscope=""
  itemtype="https://schema.org/Product"
  id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="col-md-4" data-hook="product_left_part">
      <div data-hook="product_left_part_wrap">
        <div id="product-images" data-hook="product_images">
          <div id="main-image" class="panel panel-default" data-hook="">
            <div class="panel-body text-center">
                <?php echo woocommerce_get_product_thumbnail() ?>
            </div>
          </div>
          <div id="thumbnails" data-hook="">
          </div>
        </div>
        <div data-hook="product_properties">
          <?php if ($product->is_type('simple')): ?>
          <h3 class="product-section-title">Properties</h3>
            <table id="product-properties" class="table table-striped" data-hook="">
              <tbody>
                  <tr class="even">
                    <td><strong>Product</strong></td>
                    <td><?php echo $product->get_attribute('Product'); ?></td>
                  </tr>
                  <tr class="odd">
                    <td><strong>Size </strong></td>
                    <td><?php echo $product->get_attribute('Size'); ?></td>
                  </tr>
                  <tr class="even">
                    <td><strong>Type</strong></td>
                    <td><?php echo $product->get_attribute('Type'); ?></td>
                  </tr>
                  <tr class="odd">
                    <td><strong>Suitable for</strong></td>
                    <td><?php echo $product->get_attribute('Suitable for'); ?></td>
                  </tr>
              </tbody>
            </table>
          <?php endif; ?>
          </div>
          <div data-hook="promotions">
          </div>
        </div>
      </div>
      <div class="col-md-8" data-hook="product_right_part">
        <div data-hook="product_right_part_wrap">
          <div id="product-description" data-hook="product_description">
            <h1 class="product-title" itemprop="name"><?php echo get_the_title(get_the_ID()) ?></h1>
            <div class="well" itemprop="description" data-hook="description">
              <?php the_content(); ?>
            </div>
            <div id="cart-form" data-hook="cart_form">
              <?php if ($product->is_type('simple')): ?>
                <div class="row" id="inside-product-cart-form" data-hook="inside_product_cart_form" itemprop="offers" itemscope="" itemtype="https://schema.org/Offer">
                  <div data-hook="product_price" class="col-md-5">
                  <div id="product-price">
                    <h6 class="product-section-title">Price</h6>
                    <div>
                      <span class="lead price selling" itemprop="price">
                        <?php echo woocommerce_template_single_price() ?>
                      </span>
                      <span itemprop="priceCurrency" content="GBP"></span>
                    </div>
                    <link itemprop="availability" href="https://schema.org/InStock">
                  </div>

                  <div class="add-to-cart">
                    <br>
                    <div class="input-group">
                      <?php echo woocommerce_template_single_add_to_cart() ?>
                    </div>
                  </div>
                </div>
              <?php else: ?>
                  <?php if ($product->is_type('variable')): ?>
                  <div class="row" id="inside-product-cart-form" data-hook="inside_product_cart_form" itemprop="offers" itemscope="" itemtype="https://schema.org/Offer">
                    <?php echo woocommerce_template_single_add_to_cart() ?>
                  </div>
                  <?php endif; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div id="taxon-crumbs" data-hook="" class=" five ">
          <h3 class="product-section-title">Look for similar items</h3>
          <div data-hook="product_taxons">
            <ul class="list-group" id="similar_items_by_taxon" data-hook="">
              <?php
                $posttags = get_the_terms(get_the_ID(), 'product_tag');
                if ($posttags) {
                  foreach($posttags as $tag) {
                    echo '<li class="list-group-item"><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></li>';
                  }
                }
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>