<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-inner">
	<figure class="fadesc" style="opacity: 1;">
		<img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php get_the_title() ?>">
	</figure>
	<div class="banner-txt fadesc" style="opacity: 1;">
		<div class="outer-bg">
			<div class="inner-bg">
				<aside>
					<h3 class="banner-title">
						<?php
			          		$title = str_split(get_the_title());
			          		$title_str = '';
			          		foreach ($title as $key => $value) {
			          			$title_str .= '<span style="opacity: 1;">' . $value . '</span>';
			          		}
			          		echo $title_str;
			          	?>
					</h3>
               		<?php echo get_post_custom_values('summary')[0] ?>
               	</aside>
          	</div>
      	</div>
  	</div>
</div>
<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>