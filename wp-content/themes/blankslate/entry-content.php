<div id="post" style="background: linear-gradient(
  rgba(0, 0, 0, 0.25),
  rgba(0, 0, 0, 0.25)
),url(<?php echo get_the_post_thumbnail_url() ?>) no-repeat center center fixed;background-size: cover;">
    <div class="text-vcenter">
        <h1><?php the_title(); ?></h1>
        <a href="#about" id="js-anchor-link" class="btn btn-default btn-lg ">Continue</a>
    </div>
</div>
<div class="row" id="about">
    <div class="col-md-8 col-md-offset-2 text-center post-body">
    	<?php the_content(); ?>
	</div>
</div>