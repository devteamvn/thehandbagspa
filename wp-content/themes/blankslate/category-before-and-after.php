<?php get_header(); ?>
<?php $category = get_the_category()[0]; ?>
<div class="banner-inner">
	<figure class="fadesc" style="opacity: 1;">
		<?php echo get_term_thumbnail($category->term_id) ?>
	</figure>
	<div class="banner-txt fadesc" style="opacity: 1;">
		<div class="outer-bg">
			<div class="inner-bg">
				<aside>
					<h3 class="banner-title">
						<?php
						$title = str_split($category->name);
						$title_str = '';
						foreach ($title as $key => $value) {
							$title_str .= '<span style="opacity: 1;">' . $value . '</span>';
						}
						echo $title_str;
						?>
					</h3>
					<?php echo $category->description ?>
				</aside>
			</div>
		</div>
	</div>
</div>
<div class="baa-search">
	<input type="text" id="quicksearch" placeholder="Search by make, treatment, problem.."></input>
</div>
<div class="grid baa">
	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article class="grid-item">
			<aside>
				<h4><?php the_title(); ?></h4>
			</aside>
			<figure>
				<img
					height="543"
					alt="<?php the_title(); ?>"
					src="<?php echo get_the_post_thumbnail_url() ?>"
					data-original="<?php echo get_the_post_thumbnail_url() ?>">
				</a>
			</figure>
			<aside>
				<p>
					<?php echo get_post_custom_values('summary')[0] ?>
				</p>
			</aside>
		</article>
	<?php endwhile; endif; ?>
</div>

<script>
	function defer(method) {
		if (window.jQuery)
			method();
		else
			setTimeout(function() { defer(method) }, 50);
	}
	setTimeout(function() {
		defer(function () {

			var $win = jQuery(window),
				$imgs = jQuery("img");

			$imgs.lazyload({
				failure_limit: Math.max($imgs.length - 1, 0)
			});
			// quick search regex
			var qsRegex;
			var $grid = jQuery('.grid');

			$grid.isotope({
				itemSelector: '.grid-item',
				masonry: {
					columnWidth: '.grid-sizer',
					percentPosition: true,
					gutter: '.gutter-sizer',
					transitionDuration: 0

				},
				onLayout: function() {
					$win.trigger("scroll");
				},
				filter: function() {
					return qsRegex ? $(this).text().match(qsRegex) : true;
				},
			});

			// use value of search field to filter
			var $quicksearch = jQuery('#quicksearch').keyup(debounce(function() {
				qsRegex = new RegExp($quicksearch.val(), 'gi');
				$grid.isotope();
				$imgs.trigger('appear');
			}, 200));

		});
	}, 50);

	// debounce so filtering doesn't happen every millisecond
	function debounce(fn, threshold) {
		var timeout;
		return function debounced() {
			if (timeout) {
				clearTimeout(timeout);
			}

			function delayed() {
				fn();
				timeout = null;
			}
			timeout = setTimeout(delayed, threshold || 100);
		}
	}
	;
</script>
<?php get_footer(); ?>
