		</div><!-- /container-in -->
		<footer class="footer-main">
			<aside>
				<p>©THBS Repairs t/a The Handbag Spa Ltd 2016</p>
			</aside>
			<aside>
				<p>Contact Us: <a href="tel:+441423888709">+44 (0)1423 888 709</a></p>
			</aside>
			<aside>
				<p><a href="mailto:info@thehandbagspa.com">info@thehandbagspa.com</a></p>
			</aside>
			<aside>
				<p>Opening Times: Monday - Friday 9:00am - 5:30pm </p>
			</aside>
		</footer>
	</div><!-- /container -->
</div><!-- /container-main -->
<?php wp_footer(); ?>
</body>
</html>