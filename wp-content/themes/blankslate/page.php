<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="banner-main inner">
	<div class="col-xs-6 fadesc" style="opacity: 1;">
		<figure><img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php get_the_title() ?>"></figure>
	</div>
	<div class="col-xs-6 sub fadesc" style="opacity: 1;">
		<aside class="aside2">
			<div class="outer-bg">
				<div class="inner-bg">
					<h1 class="banner-title">
			          	<?php
			          		$title = str_split(get_the_title());
			          		$title_str = '';
			          		foreach ($title as $key => $value) {
			          			$title_str .= '<span style="opacity: 1;">' . $value . '</span>';
			          		}
			          		echo $title_str;
			          	?></h1>
  					<p><?php echo get_post_custom_values('summary')[0] ?></p>
          		</div>
          	</div>
      	</aside>
    </div>
 </div>

<?php the_content(); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>