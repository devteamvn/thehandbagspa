<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head data-hook="inside_head">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>The Handbag Spa: Handbag Repair, Restoration and Cleaning - The Handbag Spa</title>
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1" name="viewport">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="description" content="The Handbag Spa is a UK based handbag cleaning and repair service for all handbags including luxury bags such as Mulberry and Hermès and many more." />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<script src="<?php echo get_template_directory_uri(); ?>/js/all.js" async="async"></script>
	<script type="text/javascript">
	 // Spoof the browser into thinking it is Retina
	 // comment the next line out to make sure it works without retina
	 window.devicePixelRatio = 2;
	</script>
	<!--[if lt IE 9]>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6/html5shiv.min.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body class="one-col" id="default" data-hook="body" <?php //body_class(); ?>>
	<div class="container-main">
		<div class="container">
			<div class="container-in">
				<!-- Header
				================================================== -->
				<header class="header-main fadesc">
					<div class="top-bar">
						<div class="social-links">
				          	<ul>
								<li><a href="https://twitter.com/thehandbagspa" target="_blank" class="fa fa-twitter"><span>twitter</span></a></li>
								<li><a href="https://www.facebook.com/thehandbagspa/" target="_blank" class="fa fa-facebook"><span>facebook</span></a></li>
								<li><a href="https://www.youtube.com/thehandbagspa/" target="_blank" class="fa fa-youtube"><span>youtube</span></a></li>
				          	</ul>
			          	</div>
			          	<div class="aside-right">
			          		<a href="/login">Login</a>
		          			<a class="cart-info empty" href="/cart">
		          				<span class='glyphicon glyphicon-shopping-cart'></span> Basket:
		          				<?php
		          					if(function_exists('WC')) {
		          						$cart_items_count = WC()->cart->get_cart_contents_count();
		          						if ($cart_items_count) {
		          							echo $cart_items_count;
		          						} else {
		          							echo 'Empty';
		          						}
		          					} else {
		          						echo 'Empty';
		          					}
	          					?>
	          				</a>
	          			</div>
          			</div>
          			<div class="nav-bar">
          				<div class="brand">
          					<a href="/" title="The Handbag spa">
          						<img src="http://thehandbagspa.com/assets/brand-the-handbag-spa-da1f8a614c123ad03e43babd82129651ada64974e9a75952b0423df4f9b6aa64.png" alt="Brand the handbag spa da1f8a614c123ad03e43babd82129651ada64974e9a75952b0423df4f9b6aa64" />
      						</a>
  						</div>
  						<a class="menu-btn" data-toggle="collapse" data-target=".menu-collapse">
  							<div class="icon-bar">
  								<span></span>
							</div>
						</a>
						<div class="menu-collapse collapse">
							<?php
									wp_nav_menu(array(
										'theme_location' => 'main-menu',
										'container' => 'nav'
									));
								?>
						</div>
					</div>
				</header>