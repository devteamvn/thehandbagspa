<?php get_header(); ?>
<div class="grid">
	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'entry' ); ?>
	<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>